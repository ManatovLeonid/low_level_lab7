#include "mem.h"

static struct mem *heap = NULL;

static void *page_init(void *address, size_t size, bool fixed) {
    if (size < HEAP_PAGE_SIZE) size = HEAP_PAGE_SIZE;
    return mmap(address, size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | (fixed ? MAP_FIXED : 0), -1, 0);
}


static void block_split(struct mem *block, size_t query) {
    struct mem *new_block = (struct mem *) ((char *) block + sizeof(struct mem) + query);

    new_block->capacity = block->capacity - sizeof(struct mem) - query;
    new_block->is_free = true;
    new_block->next = block->next;

    block->capacity = query;
    block->next = new_block;
}

static void blocks_merge(struct mem *prev, struct mem *next) {
    prev->capacity += sizeof(struct mem) + next->capacity;
    prev->next = next->next;
}

static bool blocks_sequential(struct mem *prev, struct mem *next) {
    return (char *) next == (char *) prev + sizeof(struct mem) + prev->capacity;
}

static bool block_addeable(struct mem *block, struct mem *add_block) {
    return block && add_block && add_block->is_free &&
           (blocks_sequential(block, add_block) || blocks_sequential(add_block, block)) &&
           (block->next == add_block || add_block->next == block);
}

static bool block_splittable(struct mem *block, size_t query) {
    return block->capacity >= query + sizeof(struct mem) + BLOCK_MIN_SIZE;
}

void *heap_init(size_t initial_size) {
    if (initial_size < HEAP_PAGE_SIZE) initial_size = HEAP_PAGE_SIZE;

    heap = page_init(heap, initial_size, false);
    heap->capacity = initial_size - sizeof(struct mem);
    heap->is_free = true;
    heap->next = NULL;


    return heap;
}

void *_malloc(size_t query) {
    puts("my malloc");
    struct mem *block, *prev, *new_page;
    void *page_end;
    size_t page_size;
    if (!heap) heap_init(HEAP_PAGE_SIZE);
    if (query == 0) return NULL;
    if (query < BLOCK_MIN_SIZE) query = BLOCK_MIN_SIZE;

    for (block = heap; block != NULL; block = block->next) {
        prev = block;

        if (!block->is_free) continue;

        if (block->capacity != query) {
            if (!block_splittable(block, query)) continue;
            block_split(block, query);
        }

        block->is_free = false;
        return (char *) block + sizeof(struct mem);
    }

    page_size = query + sizeof(struct mem);
    if (page_size < HEAP_PAGE_SIZE) page_size = HEAP_PAGE_SIZE;
    page_end = (char *) prev + sizeof(struct mem) + prev->capacity;


    new_page = page_init(page_end, page_size, true);
    if (new_page == MAP_FAILED) {

        new_page = page_init(page_end, page_size, false);
        if (new_page == MAP_FAILED) return NULL;
    }

    prev->next = new_page;
    new_page->capacity = page_size - sizeof(struct mem);
    new_page->is_free = false;
    new_page->next = NULL;

    if (block_splittable(new_page, query)) block_split(new_page, query);

    return (char *) new_page + sizeof(struct mem);
}

void _free(void *mem) {
    struct mem *block = (struct mem *) ((char *) mem - sizeof(struct mem));
    struct mem *next, *prev;

    if (!mem) return;
    if (block != heap) {
        for (prev = heap; prev->next && prev->next != block; prev = prev->next);
        if (prev->next != block) return;
    }

    block->is_free = true;

    next = block->next;
    /* is next free - merge */
    if (next && next->is_free && blocks_sequential(block, next)) blocks_merge(block, next);
    /* is prev free - merge */
    if (block != heap && prev->is_free && blocks_sequential(prev, block)) blocks_merge(prev, block);
}


void *_realloc(void *ptr, size_t query) {
    struct mem *header = (struct mem *) ((char *) ptr - sizeof(struct mem));
    void *new_ptr = malloc(query);
    if (new_ptr == NULL) return NULL;
    memcpy(new_ptr, ptr, header->capacity);
    free(ptr);
    return new_ptr;

}

void *_calloc(size_t num, size_t size) {
    void *new_ptr = malloc(num * size);
    if (new_ptr == NULL) return NULL;
    memset(new_ptr, 0, num * size);
    return new_ptr;

}


void *stupid_realloc(void *ptr, size_t query) {
    struct mem *header = (struct mem *) ((char *) ptr - sizeof(struct mem));
    if (header == NULL)return NULL;
    struct mem *pre_chunk = HEAP_START;


    if (header->capacity <= query) {
        if (block_splittable(header, query)) block_split(header, query);
        return ptr;
    }

    for (; pre_chunk && pre_chunk->next != header; pre_chunk = pre_chunk->next);

    if (block_addeable(header, header->next) && (
            sizeof(struct mem) + header->next->capacity + header->capacity >= query)) {
        printf("merged with after\n");

        blocks_merge(header, header->next);
        if (block_splittable(header, query))
            block_split(header, query);
        return (char *) header + sizeof(struct mem);

    } else if (block_addeable(header, pre_chunk) &&
               (pre_chunk->capacity + header->capacity + sizeof(struct mem) >= query)) {
        printf("merged with before\n");
        struct mem originCopy = {header->next, header->capacity, header->is_free};
        memcpy(pre_chunk + sizeof(struct mem), header + sizeof(struct mem), header->capacity);
        pre_chunk->capacity += originCopy.capacity + sizeof(struct mem);
        pre_chunk->next = originCopy.next;
        if (block_splittable(pre_chunk, query))
            block_split(pre_chunk, query);

        return (char *) pre_chunk + sizeof(struct mem);
    } else if (block_addeable(header, pre_chunk) && block_addeable(header, header->next) &&
               header->next->capacity + sizeof(struct mem) + header->capacity + pre_chunk->capacity +
               sizeof(struct mem) >= query) {

        printf("merged with before and after\n");
        struct mem originCopy = {header->next, header->capacity, header->is_free};
        memcpy(pre_chunk + sizeof(struct mem), header + sizeof(struct mem), header->capacity);
        pre_chunk->capacity +=
                originCopy.capacity + sizeof(struct mem) + sizeof(struct mem) + originCopy.next->capacity;
        pre_chunk->next = originCopy.next->next;

        if (block_splittable(pre_chunk, query))
            block_split(pre_chunk, query);

        return (char *) pre_chunk + sizeof(struct mem);

    } else {
        printf("reallocated in new place\n\n");
        void *newdata = malloc(query);
        memcpy(newdata, header + sizeof(struct mem), header->capacity);
        free(ptr);
        return (char *) newdata;
    }
}


void memalloc_debug_struct_info(FILE *f, struct mem const *const address) {
    size_t i;
    char *ptr = (char *) address + sizeof(struct mem);

    fprintf(f, "start: %p, size: %4lu, is_free: %d, bytes:", (void *) address, address->capacity, address->is_free);
    for (i = 0; i < DEBUG_FIRST_BYTES && i < address->capacity; i++, ptr++) {
        fprintf(f, " %2hhX", *ptr);
    }
    putc('\n', f);
}

void memalloc_debug_heap(FILE *f) {
    struct mem *ptr = heap;
    for (; ptr; ptr = ptr->next) memalloc_debug_struct_info(f, ptr);
}
